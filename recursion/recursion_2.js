function binaryRec(arr,x,start,end){
    if(start>end){return false;}
    var mid= Math.floor((start+end)/2);
    if(arr[mid]===x){return mid;}
    if(arr[mid]>x){
       return binaryRec(arr,x,start,mid-1);
    }else{
        return binaryRec(arr,x,mid+1,end);
    }
}
var arr=[1,5,6,7,9];
var x=9;

console.log("position of given element is "+(binaryRec(arr,x,0,arr.length-1)+1));