
const arr= [5,3,8];

//forEach
var sum=0;
function adding(i){
    sum+=i;
}
arr.forEach(adding);
console.log(sum);

//forIn
var total=0;
for(var x in arr){
    total+=arr[x];
}
console.log(total);

//for
var sumTotal=0;
for(var i=0;i<arr.length;i++){
    sumTotal+=arr[i];
}
console.log(sumTotal);