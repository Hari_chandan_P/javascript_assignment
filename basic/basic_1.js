var today = new Date();
var day = today.getDay();

var week = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
console.log("Today is : "+week[day]+".");

var hour = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
var ampm = (hour>=12)?"PM":"AM";
hour = (hour>=12)?(hour-12):hour;
if(hour===0 && ampm==="PM"){
    if(min===0 && sec===0){
        ampm = "noon";
        hour=12;
    }
    else{
        hour= 12;ampm = "PM";
    }
}
if(hour===0 && ampm==="AM"){
    if(min===0 && sec===0){
        ampm = "midnight";
        hour=12;
    }
    else{
        hour= 12;ampm = "AM";
    }
}
console.log("Current Time is : "+hour+" "+ampm+" : "+min+" : "+sec);